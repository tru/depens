mkdir -p /home/centos/.ssh

echo 'ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAxvYO6o1t+Pjc2aD58A60i7AUqCmy7ikCl8+AEH2VnNpFyZq55GNpae9SKt8s38zxK6sRWxpVvUfabQwRwB8SXq89PZyDCv6kcRcqIJxrsxdDqZh30mKd31mIj/EyAi6N7WFns5IUFa/LI1u2wN7gFJR1FfTT1RVr9vrsVqKjo+Aa13RLQnvs/RWzwLXUPR0T0XO5BkPD6ssuf3EjE6lEUS4QzVToof/QQzpylrDIDCEzxE++sQk2jgLk77f+S9RAhwNiRALMHZAKRZ0WrTB9lCLh09hbdBeehKhzJ9ygIA28zrqXE2NjoMrDP0XmVncYpzwP3wVM/sJZ9lx0L8XXDw== depens-centos' > /home/centos/.ssh/authorized_keys

chmod 0700 /home/centos/.ssh
chmod 0600 /home/centos/.ssh/authorized_keys
chown -R centos:centos /home/centos/.ssh
restorecon -rv /home/centos
echo 'centos ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
restorecon -rv /etc/sudoers
