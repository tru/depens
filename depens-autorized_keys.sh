mkdir -p /home/depens/.ssh

echo 'ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAywxzBAhpXltfF2Gv5s4m+LeMBcimFJDHgQPRD1RvbH+WvZJnSrebwAFryg9KqXbGLEqQOtYqbC6SnlwsuanlWFw3kWNBBz7xs7NdxrIRQ+lMNd1X5ckf34VWDk7CR+Yk7fMro70z1M0rK1K5Z/TzXOZoELXlHR5JJGnaTIidkK/wPQq+VOGvUMMXZyD7dPFfQJZXtn8ThGf5wJY6/r+KXyMy9CNUJszZCVVTfd3JJNU8ZtF1hLizEtMrH5m1Q+sSGosL7UgTqFjPaxbk/zdne77gaQ7WkK4jg25BnCkPoXxSHeCFcP0yQFYeESP2n/89G0xF4BTqOCkc1HRpwIl6kw== depens-student' >/home/depens/.ssh/authorized_keys

chmod 0700 /home/depens/.ssh
chmod 0600 /home/depens/.ssh/authorized_keys
chown -R depens:depens /home/depens/.ssh
restorecon -rv /home/depens
